package com.skytribe.sharify.application;

import javax.inject.Singleton;

import dagger.Component;
import com.skytribe.sharify.feature.lobby.LobbyPresenter;
import com.skytribe.sharify.feature.login.LoginPresenter;
import com.skytribe.sharify.feature.login.SpotifyAuthenticationActivity;
import com.skytribe.sharify.feature.party.PartyPresenter;
import com.skytribe.sharify.feature.party.partymember.PartyMemberFragment;
import com.skytribe.sharify.feature.party.settings.SettingsFragment;
import com.skytribe.sharify.feature.party.tracklist.TracklistFragment;
import com.skytribe.sharify.feature.search.SearchPresenter;
import com.skytribe.sharify.feature.search.playlistsearch.PlaylistSearchPresenter;
import com.skytribe.sharify.feature.search.songsearch.SongSearchPresenter;
import com.skytribe.sharify.service.player.SpotiqHostService;

@Singleton
@Component(modules = { AppModule.class })
public interface AppComponent {
    void inject(SpotifyAuthenticationActivity target);
    void inject(LoginPresenter target);
    void inject(LobbyPresenter target);
    void inject(PartyPresenter target);
    void inject(SearchPresenter target);
    void inject(TracklistFragment target);
    void inject(PartyMemberFragment target);
    void inject(SettingsFragment target);
    void inject(SongSearchPresenter target);
    void inject(PlaylistSearchPresenter target);
    void inject(SpotiqHostService target);
}
