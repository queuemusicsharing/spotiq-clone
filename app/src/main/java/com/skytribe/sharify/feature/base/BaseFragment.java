package com.skytribe.sharify.feature.base;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;

//import com.github.andrewlord1990.snackbarbuilder.SnackbarBuilder;

import nucleus5.factory.PresenterFactory;
import nucleus5.presenter.Presenter;
import nucleus5.view.NucleusSupportFragment;
import com.skytribe.sharify.constant.ApplicationConstants;
import com.skytribe.sharify.util.di.Injector;

public abstract class BaseFragment<P extends Presenter> extends NucleusSupportFragment<P> implements BaseView {

    private boolean snackbarShowing = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final PresenterFactory<P> superFactory = super.getPresenterFactory();
        setPresenterFactory(superFactory == null ? null : (PresenterFactory<P>) () -> {
            P presenter = superFactory.createPresenter();
            ((Injector) getActivity().getApplication()).inject(presenter);
            return presenter;
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public boolean onBackPressed() {
        return false;
    }

    public void showMessage(String message) {
        if (snackbarShowing) {
            deferMessage(message);
        }
        else {
            Snackbar.make(getRootView(), message, Snackbar.LENGTH_LONG)
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            snackbarShowing = false;
                        }
                        @Override
                        public void onShown(Snackbar snackbar) {
                            snackbarShowing = true;
                        }
                    })
                    .show();
        }
    }

    private void deferMessage(String message) {
        new Handler().postDelayed(() -> {
            Snackbar.make(getRootView(), message, Snackbar.LENGTH_LONG)
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar snackbar, int event) {
                            snackbarShowing = false;
                        }
                        @Override
                        public void onShown(Snackbar snackbar) {
                            snackbarShowing = true;
                        }
                    })
                    .show();
        }, ApplicationConstants.DEFER_SNACKBAR_DELAY);
    }

    @Override
    public void finishWithSuccess(String message) {
        ((BaseView) getActivity()).finishWithSuccess(message);
    }

    @Override
    public View getRootView() {
        return ((BaseView) getActivity()).getRootView();
    }
}
