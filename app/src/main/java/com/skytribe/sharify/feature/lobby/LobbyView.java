package com.skytribe.sharify.feature.lobby;

import com.skytribe.sharify.feature.base.BaseView;

public interface LobbyView extends BaseView {
    void setUserDetails(String userName, String userImageUrl);
    void goToParty(String partyTitle);

}
