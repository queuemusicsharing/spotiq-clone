package com.skytribe.sharify.feature.login;

import com.skytribe.sharify.feature.base.BaseView;

public interface LoginView extends BaseView {
    void startProgress();
    void finishProgress();
    void resetProgress();
    void goToSpotifyAuthentication();
    void goToLobby();
}
