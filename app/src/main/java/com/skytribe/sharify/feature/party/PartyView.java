package com.skytribe.sharify.feature.party;

import com.skytribe.sharify.feature.base.BaseView;


public interface PartyView extends BaseView {
    void setUserDetails(String userName, String userImageUrl);
    void setHostPrivileges();
}
