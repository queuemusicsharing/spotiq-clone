package com.skytribe.sharify.feature.party.tracklist;

import com.skytribe.sharify.feature.base.BaseView;
import com.skytribe.sharify.model.Song;

public interface TracklistView extends BaseView {
    void addSong(Song song);
    void removeSong(Song song);
}
