package com.skytribe.sharify.feature.search;

import com.skytribe.sharify.model.Song;

public interface SearchFragmentParent {
    void addRequest(Song song);
    void removeRequest(Song song);
}
