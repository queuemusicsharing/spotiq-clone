package com.skytribe.sharify.feature.search;

import java.util.ArrayList;

import com.skytribe.sharify.feature.base.BaseView;
import com.skytribe.sharify.model.Song;

public interface SearchView extends BaseView {
    void updateRequestList(ArrayList<Song> songRequests);
    void updateSongRequestsLabel();
    void finishRequest();
}
