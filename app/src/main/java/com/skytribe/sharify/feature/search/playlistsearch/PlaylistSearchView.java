package com.skytribe.sharify.feature.search.playlistsearch;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;
import com.skytribe.sharify.feature.base.BaseView;
import com.skytribe.sharify.model.Song;

public interface PlaylistSearchView extends BaseView {
    void updatePlaylists(List<PlaylistSimple> playlists);
    void updateSongs(List<Song> songs);
}
