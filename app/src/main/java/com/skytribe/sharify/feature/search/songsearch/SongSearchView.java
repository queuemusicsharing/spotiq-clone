package com.skytribe.sharify.feature.search.songsearch;

import java.util.List;

import com.skytribe.sharify.feature.base.BaseView;
import com.skytribe.sharify.model.Song;

public interface SongSearchView extends BaseView {
    void updateSearch(List<Song> songs);
    void updateSearchSuggestions(SongSearchSuggestionsBuilder searchSuggestionsBuilder);
}
