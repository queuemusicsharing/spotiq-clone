package com.skytribe.sharify.util;

import com.skytribe.sharify.BuildConfig;

public class VersionUtil {

    public static int getCurrentAppVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

}
