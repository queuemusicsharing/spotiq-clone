package com.skytribe.sharify.util.di;

public interface Injector {
    void inject(Object target);
}
