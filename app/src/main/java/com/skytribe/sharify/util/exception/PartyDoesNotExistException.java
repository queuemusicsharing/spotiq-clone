package com.skytribe.sharify.util.exception;

public class PartyDoesNotExistException extends Exception {

    public PartyDoesNotExistException() {
        super("Party does not exist");
    }

}
