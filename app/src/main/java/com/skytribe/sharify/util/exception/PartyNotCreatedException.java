package com.skytribe.sharify.util.exception;

public class PartyNotCreatedException extends Exception {

    public PartyNotCreatedException() {
        super("Party was not created");
    }

}
