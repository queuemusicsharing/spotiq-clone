package com.skytribe.sharify.util.exception;

public class PartyVersionHigherException extends Exception {

    public PartyVersionHigherException() {
        super("Party version is higher than user version");
    }

}
