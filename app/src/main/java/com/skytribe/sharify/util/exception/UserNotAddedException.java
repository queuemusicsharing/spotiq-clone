package com.skytribe.sharify.util.exception;

public class UserNotAddedException extends Exception {

    public UserNotAddedException() {
        super("User was not added");
    }

}
