package com.skytribe.sharify.util.validator;

import android.support.annotation.NonNull;

import com.rengwuxian.materialedittext.validation.METValidator;
import com.skytribe.sharify.constant.ValidationConstants;

public class PartyPasswordMatchValidator extends METValidator {

    String passwordMatch;

    public PartyPasswordMatchValidator(String passwordToMatch) {
        super(ValidationConstants.PARTY_PASSWORD_MATCH_ERROR_MESSAGE);
        this.passwordMatch = passwordToMatch;
    }

    @Override
    public boolean isValid(@NonNull CharSequence charSequence, boolean b) {
        return charSequence.toString().matches(passwordMatch);
    }
}
